<?php

namespace kirill\parser;

/**
 * @author Kirill
 */
interface ParserInterface
{
    /**
     * 
     * @param string $url
     * @param string $tag
     */
    public function process(string $url, string $tag): array;
}

